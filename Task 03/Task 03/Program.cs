﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_03
{
    class Program
    {
        static void Main(string[] args)
        {
            number(8, 9);
        }
        static void number(int a,int b)
        {
            Console.WriteLine($"{a}+{b} = {a+b}");
            Console.WriteLine($"{a}-{b} = {a-b}");
            Console.WriteLine($"{a}/{b} = {a/b}");
            Console.WriteLine($"{a}*{b} = {a*b}");
        }
    }
}
